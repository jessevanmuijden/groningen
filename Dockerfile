FROM node:12.22-alpine As development-ui

WORKDIR /usr/src/app
COPY ./client/package*.json ./
RUN npm install --only=development
COPY ./client .
RUN npm run build


FROM node:12.22-alpine as production-ui

ARG NODE_ENV=production
ENV NODE_ENV=${NODE_ENV}

WORKDIR /usr/src/app
COPY ./client/package*.json ./
RUN npm install --only=production
COPY --from=development-ui /usr/src/app/dist ./dist


FROM node:12.22-alpine As development-api

WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install --only=development
COPY . .
RUN npm run build


FROM node:12.22-alpine as production-api

ARG NODE_ENV=production
ENV NODE_ENV=${NODE_ENV}

WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install --only=production
COPY --from=development-api /usr/src/app/dist ./dist
COPY --from=production-ui /usr/src/app/dist ./client/dist

CMD ["node", "dist/main"]