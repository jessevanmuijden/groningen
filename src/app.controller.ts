import { Body, Controller, Get, Headers, Param, Post, Put } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
  ) {}

  @Get('processes')
  getProcesses(): Promise<any> {
    return this.appService.getProcesses();
  }

  @Post('processInstances')
  createProcessInstance(@Body() body: any): Promise<any> {
    return this.appService.createProcessInstance(body);
  }

  @Get('processInstances/:id/status')
  getProcessInstanceStatus(
    @Param('id') id: string,
    @Headers('Authorization') Authorization: string
  ): Promise<any> {
    return this.appService.getProcessInstanceStatus(id, Authorization)
  }
  
  @Get('processInstances/:id/activities')
  getProcessInstanceActivities(
    @Param('id') id: string,
    @Headers('Authorization') Authorization: string
  ): Promise<any> {
    return this.appService.getProcessInstanceActivities(id, Authorization)
  }
  
  @Get('processInstances/:id/variables')
  getProcessInstanceVariables(
    @Param('id') id: string,
    @Headers('Authorization') Authorization: string
  ): Promise<any> {
    return this.appService.getProcessInstanceVariables(id, Authorization)
  }


  @Put('processInstances/:id/variables')
  putProcessInstanceVariables(
    @Param('id') id: string,
    @Body() body: any,
    @Headers('Authorization') Authorization: string
  ): Promise<any> {
    return this.appService.putProcessInstanceVariables(id, body, Authorization)
  }
}
