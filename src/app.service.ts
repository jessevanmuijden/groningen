import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import axios from 'axios';
import jwt_decode from 'jwt-decode';

@Injectable()
export class AppService {
  
  static readonly PATH_LOGIN: string = '/auth/local';
  static readonly PATH_PROCESSES: string = '/proxy/processes';
  static readonly PATH_PROCESS_INSTANCES: string = '/proxy/processInstance';
  static readonly PATH_PROCESS_INSTANCE_STATUS: string = '/proxy/status/';
  static readonly PATH_PROCESS_INSTANCE_ACTIVITIES: string = '/proxy/activities/';
  static readonly PATH_PROCESS_INSTANCE_VARIABLES: string = '/proxy/variables/';

  static token: string = '';

  constructor(
    private readonly configService: ConfigService,
  ) {}

  async getProcesses(): Promise<any> {
    const { data } = await axios.get(AppService.PATH_PROCESSES, {
      headers: {
        Authorization: `Bearer ${await this.getToken()}`
      }
    })
    return data
  }
  
  async createProcessInstance(body: any): Promise<any> {
    const { data } = await axios.post(
      AppService.PATH_PROCESS_INSTANCES,
      body,
      { headers: { Authorization: `Bearer ${await this.getToken()}` } }
    )
    return data
  }

  async getProcessInstanceStatus(id: string, Authorization: string): Promise<any> {
    const { data } = await axios.get(`${AppService.PATH_PROCESS_INSTANCE_STATUS}${id}`, {
      headers: { Authorization },
    })
    return data
  }

  async getProcessInstanceActivities(id: string, Authorization: string): Promise<any> {
    const { data } = await axios.get(`${AppService.PATH_PROCESS_INSTANCE_ACTIVITIES}${id}`, {
      headers: { Authorization },
    })
    return data
  }

  async getProcessInstanceVariables(id: string, Authorization: string): Promise<any> {
    const { data } = await axios.get(`${AppService.PATH_PROCESS_INSTANCE_VARIABLES}${id}`, {
      headers: { Authorization },
    })
    return data
  }
  
  async putProcessInstanceVariables(id: string, body: any, Authorization: string): Promise<any> {
    const { data } = await axios.put(`${AppService.PATH_PROCESS_INSTANCE_VARIABLES}${id}`, body, {
      headers: { Authorization },
    })
    return data
  }

  async getToken(): Promise<string> {
    if (AppService.token) {
      const token = jwt_decode<any>(AppService.token)
      if (token.exp && new Date(token.exp * 1000) > new Date()) {
        return AppService.token
      }
    }

    try {
      const { data } = await axios.post(AppService.PATH_LOGIN, {
        identifier: this.configService.get<string>('API_IDENTIFIER'),
        password: this.configService.get<string>('API_PASSWORD'),
      })

      AppService.token = data.jwt
    } catch (e) {
      console.log(e)
    }

    return AppService.token
  }
}
