import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ConfigService } from '@nestjs/config';
import axios from 'axios';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.setGlobalPrefix('api');
  const configService = app.get(ConfigService);
  axios.defaults.baseURL = configService.get<string>('API_BASE_URL');
  if (configService.get<string>('NODE_ENV') !== 'production') {
    app.enableCors();
  }
  await app.listen(configService.get<number>('SERVER_PORT'));
}
bootstrap();
