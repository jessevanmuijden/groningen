import axios from 'axios'

export default {
  async fetchProcesses ({commit}) {
    try {
      const {data} = await axios.get('/processes')
      const processes = data.data.map(p => {
        p.steps = p.steps.map(s => {
          const step = s.step
          step.variables = step.variables.map(v => {
            return v.variable
          })
          step.connections = step.connections.map(c => {
            return c.connection
          })
          step.decisionTables = step.decisionTables.map(d => {
            return d.decisionTable
          })
          step.conditions = step.conditions.map(c => {
            return c.condition
          })
          return step
        })
        return p
      })
      commit('setProcesses', processes)
    } catch (e) {
      console.log(e)
    }
  }
}
