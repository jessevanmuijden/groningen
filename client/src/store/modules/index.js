import common from './common'
import processes from './processes'

export default {
  common,
  processes,
}