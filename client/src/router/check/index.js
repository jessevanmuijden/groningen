import CheckView from '../../views/CheckView.vue'

export default [
  {
    path: '/check/:slug',
    name: 'Check',
    component: CheckView
  }
]