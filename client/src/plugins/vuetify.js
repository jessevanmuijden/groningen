import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: '#e60004',
        secondary: '#BF7717',
        tertiary: '#BC2ABF',
        background: '#F0F5F9',
      },
    },
    options: {
      customProperties: true,
    },
  },
});
