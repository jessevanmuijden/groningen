# Groningen client for Ketting

## Description

NestJS server with Vue.js client

## Installation

```bash
$ npm install
$ cd client
$ npm install
```

## Running the server

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Running the client

```bash
# watch mode
$ npm run serve

# production mode
$ npm run build
```

## Run with docker-compose

```bash
$ docker-compose up -d
```